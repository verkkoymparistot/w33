import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birds from './bird.json';
import App from './App.css'

const Bird = () => {
    function compare( a, b ) {
        if ( a.finnish < b.finnish ){
          return -1;
        }
        if ( a.finnish > b.finnish ){
          return 1;
        }
        return 0;
      }
      birds.sort( compare );

    return(
        <div>
            <TableContainer component={Paper}>
                <Table  sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead className='otsikko'>
                    <TableRow>
                        <TableCell><b>Finnish</b></TableCell>
                        <TableCell align="left"><b>Swedish</b></TableCell>
                        <TableCell align="left"><b>English</b></TableCell>
                        <TableCell align="left"><b>Short</b></TableCell>
                        <TableCell align="left"><b>Latin</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {birds.map((birds) => (
                        <TableRow key={birds.finnish}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="birds">
                            {birds.finnish}
                        </TableCell>
                        <TableCell align="left">{birds.swedish}</TableCell>
                        <TableCell align="left">{birds.english}</TableCell>
                        <TableCell align="left">{birds.short}</TableCell>
                        <TableCell align="left">{birds.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )

}


export default Bird;